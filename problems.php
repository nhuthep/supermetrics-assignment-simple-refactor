<?php

// Using $_POST instead of $_REQUEST =>
if ($_REQUEST['email']) { // Make sure we have key email exist first, check by isset or key_exists
    $masterEmail = $_REQUEST['email']; // Declare new variable in condition statement, if the condition return false,
    // the blocks outside will warning Undefined. Every times we try to access the variable value, we have to check isset.
    // To avoid that, maybe we can have a default value $a = condition ? 'something' : 'default'
    // or longer expression like $a = 'default'; if ($condition) $a = 'something'
}

// Un-parenthesized `a ? b : c ? d : e` is not supported from version 7.4.0. Use either `(a ? b : c) ? d : e` or `a ? b : (c ? d : e)`
// For this specific example, we can check with 2 cases:

// Suppose we use (a ? b : c) ? d : e => in case a true => (a ? b : c) ? d : e will be b ? d : e and of course with a true, b must be true.
// So b ? d : e will return d.
// But if c is fail, d must be false: null or empty string or 0/"0" -> therefore the final result will be blank or 0/"0"

// Suppose we use a ? b : (c ? d : e). Similar with above approach we can see it make more sense in any cases.
$masterEmail = isset($masterEmail) && $masterEmail
    ? $masterEmail
    : array_key_exists('masterEmail', $_REQUEST) && $_REQUEST["masterEmail"] // Using single quoted here is better than double quoted
        ? $_REQUEST['masterEmail'] : 'unknown';

echo 'The master email is ' . $masterEmail . '\n'; // Only double quoted strings interpret the escape sequences \n

// Before query we can validate $masterEmail is a valid email.
// We can archive that with preg_match and regEx or with filter_var and FILTER_VALIDATE_EMAIL filter

// Perform mysqli connect, query and fetch row... will appear many times
// => so we can extract this behavior to a function to avoid repeated code.
$conn = mysqli_connect('localhost', 'root', 'sldjfpoweifns', 'my_database');

// Should check database connection success before performing query
// Potential of SQL injection by using direct user input
$res = mysqli_query($conn, "SELECT * FROM users WHERE email='" . $masterEmail . "'");

$row = mysqli_fetch_row($res);

//We should close the connection for better performance even thought it's optional

// mysqli_fetch_row will return array|null|false => must to check value before access
// even $row is array, we still doesn't have key 'username' directly => do an iterator then we can access username from item
echo $row['username'] . "\n";


//================================== REFACTORED CODE ===================================================

$masterEmail = $_REQUEST['email'] ?? $_REQUEST['masterEmail'] ?? 'unknown';

echo "The master email is $masterEmail\n";

try {
    $mysqli = new mysqli('localhost', 'root', 'sldjfpoweifns', 'my_database');
} catch (Exception $exception) {
    echo "Failed to connect to MySQL";
    exit();
}

$sql = 'SELECT * FROM users WHERE email= ?';

$stmt = $mysqli->prepare($sql);
$stmt->bind_param("s", $masterEmail);
$stmt->execute();

$result = $stmt->get_result();

while ($rows = $result->fetch_assoc()) {
    printf("%s\n", $row['username']);
}

$mysqli->close();



