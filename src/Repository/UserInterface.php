<?php

namespace ThepTest\Repository;

/**
 * Interface UserInterface
 *
 * @package ThepTest\Repository
 */
interface UserInterface
{
    /**
     * @param string $email
     * @return array
     */
    public function getUsersByEmail(string $email): array;
}
