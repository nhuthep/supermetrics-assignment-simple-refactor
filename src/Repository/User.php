<?php

namespace ThepTest\Repository;

use ThepTest\Driver\ConnectorInterface;
use ThepTest\Dto\DataTransformerInterface;
use ThepTest\Dto\MysqliResultTransformer;

/**
 * Class User
 *
 * @package ThepTest\Repository
 */
class User implements UserInterface
{
    /**
     * @var ConnectorInterface
     */
    private $connector;

    /**
     * @var MysqliResultTransformer
     */
    private $transformer;

    /**
     * @var string
     */
    private string $table = 'users';

    /**
     * User constructor.
     *
     * @param ConnectorInterface $connector
     * @param DataTransformerInterface $transformer
     */
    public function __construct(ConnectorInterface $connector, DataTransformerInterface $transformer)
    {
        $this->connector = $connector;
        $this->transformer = $transformer;
    }

    /**
     * @param string $email
     * @return array
     */
    public function getUsersByEmail(string $email): array
    {
        // todo: may have query builder to generate query
        $sql = 'SELECT * FROM ' . $this->table . ' WHERE email = ?';

        $result = $this->connector->perform($sql, [$email]);

        return $this->transformer->transform($result);
    }
}
