<?php

namespace ThepTest\Dto;

/**
 * Class MysqliResultTransformer
 *
 * @package ThepTest\Dto
 */
class MysqliResultTransformer implements DataTransformerInterface
{
    /**
     * @param $data
     * @return array
     */
    public function transform($data): array
    {
        return $data->fetch_array(MYSQLI_BOTH) ?: [];
    }
}
