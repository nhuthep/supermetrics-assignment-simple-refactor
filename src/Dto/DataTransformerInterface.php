<?php

namespace ThepTest\Dto;

/**
 * Interface DataTransformerInterface
 *
 * @package ThepTest\Dto
 */
interface DataTransformerInterface
{
    /**
     * @param $data
     * @return array
     */
    public function transform($data): array;
}
