<?php

namespace ThepTest\Helper;

/**
 * Class Validator
 *
 * @package ThepTest\Helper
 */
class Validator
{
    /**
     * @param string $email
     * @return bool
     */
    public static function isEmail(string $email): bool
    {
        // todo: write logic to check is valid email or not

        return !!$email;
    }
}
