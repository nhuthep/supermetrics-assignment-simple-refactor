<?php

namespace ThepTest\Exception;

use RuntimeException;

/**
 * Class ValidationException
 *
 * @package ThepTest\Exception
 */
class ValidationException extends RuntimeException
{

}
