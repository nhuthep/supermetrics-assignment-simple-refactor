<?php


namespace ThepTest\Exception;

use RuntimeException;

/**
 * Class DatabaseConnectionException
 * @package ThepTest\Exception
 */
class DatabaseConnectionException extends RuntimeException
{

}
