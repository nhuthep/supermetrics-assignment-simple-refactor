<?php

namespace ThepTest\Controller;

use ThepTest\Exception\ValidationException;
use ThepTest\Helper\Validator;
use ThepTest\Repository\UserInterface;

/**
 * Class UserController
 *
 * @package ThepTest\Controller
 */
class UserController
{
    /**
     * @var UserInterface
     */
    private $userRepo;

    /**
     * UserController constructor.
     *
     * @param UserInterface $user
     */
    public function __construct(UserInterface $user)
    {
        $this->userRepo = $user;
    }

    /**
     * @param string $email
     * @return array
     */
    public function getUsersByEmail(string $email): array
    {
        if (!Validator::isEmail($email)) {
            throw new ValidationException($email . ' is not a valid email address.');
        }

        $users = $this->userRepo->getUsersByEmail($email);

        return $users;
    }
}
