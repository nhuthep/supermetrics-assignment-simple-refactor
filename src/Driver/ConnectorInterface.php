<?php

namespace ThepTest\Driver;

/**
 * Interface ConnectorInterface
 *
 * @package ThepTest\Driver
 */
interface ConnectorInterface
{
    /**
     * @param string $query
     * @param array $params
     * @return mixed
     */
    public function perform(string $query, array $params = []);
}
