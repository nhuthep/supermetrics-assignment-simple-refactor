<?php

namespace ThepTest\Driver;

use mysqli;
use mysqli_result;
use ThepTest\Exception\DatabaseConnectionException;
use Exception;

/**
 * Class MysqlConnector
 *
 * @package ThepTest\Driver
 */
class MysqlConnector implements ConnectorInterface
{
    /**
     * @var mysqli
     */
    private $mysqli;

    /**
     * MysqlConnector constructor.
     *
     * @param string $host
     * @param string $username
     * @param string $password
     * @param string $databaseName
     */
    public function __construct(string $host, string $username, string $password, string $databaseName)
    {
        try {
            $this->mysqli = new mysqli($host, $username, $password, $databaseName);
        } catch (Exception $exception) {
            // todo: maybe log error
            throw new DatabaseConnectionException('Failed to connect to MySQL.');
        }
    }

    /**
     * @param string $query
     * @param array $params
     * @return mysqli_result|null
     */
    public function perform(string $query, array $params = []): ?mysqli_result
    {
        $result = $this->executeQuery($query, $params);

        $this->closeConnection();

        return $result;
    }

    /**
     * @param string $query
     * @param mixed ...$params
     * @return mysqli_result|null
     */
    private function executeQuery(string $query, array $params = []): ?mysqli_result
    {
        $stmt = $this->mysqli->prepare($query);
        if (count($params)) {
            $stmt->bind_param($this->prepareTypes($params), ...$params);
        }

        $stmt->execute();

        return $stmt->get_result();
    }

    /**
     * Close a previously opened database connection
     */
    private function closeConnection(): void
    {
        $this->mysqli->close();
    }

    /**
     * @param array $params
     * @return string
     */
    private function prepareTypes(array $params = []): string
    {
        $types = '';
        foreach ($params as $param) {
            $types .= is_numeric($param) ? 'i' : 's';
        }

        return $types;
    }
}
